const mongoose = require('mongoose');

const SchemaChat = new mongoose.Schema({
  parentMessage: String,
  date: Date,
  group: String,
  event: String,
  Content: String,
  user: String
}, {
  collection: 'chat',
  minimize: false,
  versionKey: false
}).set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id

    delete ret._id;
  }
})

module.exports = SchemaChat;
