const mongoose = require('mongoose');

const SchemaEvent = new mongoose.Schema({
  name: String,
  description: String,
  startDate: Date,
  endDate: Date,
  city: String,
  picture: String,
  private: Boolean,
  memberList: Array,
  adminList: Array
}, {
  collection: 'event',
  minimize: false,
  versionKey: false
}).set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id

    delete ret._id;
  }
})

module.exports = SchemaEvent;
