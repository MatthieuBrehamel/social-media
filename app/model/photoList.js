const mongoose = require('mongoose');

const SchemaPhotoList = new mongoose.Schema({
  event: String,
  user: String,
  photoList: Array,
}, {
  collection: 'photoList',
  minimize: false,
  versionKey: false
}).set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id

    delete ret._id;
  }
})

module.exports = SchemaPhotoList;
