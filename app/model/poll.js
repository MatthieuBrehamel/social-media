const mongoose = require('mongoose');

const SchemaPoll = new mongoose.Schema({
  event: String,
  question: Array,
  answer: Array,
}, {
  collection: 'poll',
  minimize: false,
  versionKey: false
}).set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id

    delete ret._id;
  }
})

module.exports = SchemaPoll;
