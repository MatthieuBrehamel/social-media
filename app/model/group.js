const mongoose = require('mongoose');

const SchemaGroup = new mongoose.Schema({
  name: String,
  description: String,
  icon: String,
  picture: String,
  type: String,
  memberCanPost: Boolean,
  memberCanEvent: Boolean,
  memberList: Array,
  adminList: Array
}, {
  collection: 'group',
  minimize: false,
  versionKey: false
}).set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id

    delete ret._id;
  }
})

module.exports = SchemaGroup;
