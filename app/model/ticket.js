const mongoose = require('mongoose');

const SchemaTicket = new mongoose.Schema({
  name: String,
  price : String,
  quantity: String,
}, {
  collection: 'ticket',
  minimize: false,
  versionKey: false
}).set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id

    delete ret._id;
  }
})

module.exports = SchemaTicket;
