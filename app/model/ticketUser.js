const mongoose = require('mongoose');

const SchemaTicketUser = new mongoose.Schema({
  firstName: String,
  lastName: String,
  adress : String,
  buyingDate : Date,
  user: String,
  ticket: String
}, {
  collection: 'ticketUser',
  minimize: false,
  versionKey: false
}).set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id

    delete ret._id;
  }
})

module.exports = SchemaTicketUser;
