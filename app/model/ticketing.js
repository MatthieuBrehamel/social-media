const mongoose = require('mongoose');

const SchemaTicketing = new mongoose.Schema({
  event: String,
  ticketType: Array,
}, {
  collection: 'ticketing',
  minimize: false,
  versionKey: false
}).set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id

    delete ret._id;
  }
})

module.exports = SchemaTicketing;
