module.exports = {
  development: {
    express: {
      type: 'development',
      port: 3000,
    },
    mongodb: {
      host: 'mongodb://127.0.0.1:27017/facebook'
    }
  },
  production: {
    express: {
      type: 'production',
      port: 3000
    },
    mongodb: {
      host: 'mongodb://127.0.0.1:27017/facebook'
    }
  }
}
