const PhotoListModel = require('../model/photoList')
const EventModel = require('../model/event')

module.exports = class PhotoList {
  constructor(app, connect) {
    this.app = app;
    this.PhotoListModel = connect.model('PhotoListModel', PhotoListModel)
    this.EventModel = connect.model('EventModel', EventModel)

    this.run();
  }

  findEvent(idEvent, res) {
    return this.EventModel.findById(idEvent).catch((err) => {
      res.status(404).json({
        status: 404,
        message: 'Event not found'
      })

      return null;
    });
  }

  create() {
    this.app.post('/photo-list/event/:id/user/:idUser', async (req, res) => {
      try {
        let groupData = '';

        groupData = await this.findEvent(req.params.id, res);

        let obj = req.body;

        if (groupData !== null) {
          let flag = false;

          if (groupData.adminList.includes(req.params.idUser)) {
            flag = true;
            obj = Object.assign(req.body, { user : req.params.idUser });
          }

          if (groupData.memberList.includes(req.params.idUser)) {
            flag = true;

            obj = Object.assign(req.body, { user : req.params.idUser });
          }

          if (false === flag) {
            res.status(404).json({
              code: 404,
              message: 'Not in event'
            })

            return null;
          }

          obj = Object.assign(req.body, { event : groupData._id });

          const photoListModel = new this.PhotoListModel(obj);

          photoListModel.save().then((photoList) => {
            res.status(200).json(photoList || {})
          }).catch(() => {
            res.status(200).json({})
          })
        }
      } catch (err) {
        console.log(`[ERROR] photoList/create -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  delete() {
    this.app.delete('/photo-list/delete/:id/user/:idUser', async (req, res) => {
      if (!req.params.id) {
        res.status(400).json({
          status: 400,
          message: 'Bad Request : Please use a id in the query string parameter'
        })

        return;
      }

      try {
        this.PhotoListModel.findById(req.params.id, function (err, doc) {
          if (doc.user === req.params.idUser) {
            doc.deleteOne(
              req.body,
              // options
            ).then((photoListUpdated) => {
              res.status(200).json(photoListUpdated || {})
            }).catch((err) => {
              res.status(400).json({
                status: 400,
                message: err
              })
            })
          } else {
            res.status(403).json({
              status: 403,
              message: 'You are not the owner'
            })
          }
        })
      } catch (err) {
        console.log(`[ERROR] photoList/delete -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  show() {
    this.app.get('/photo-list/show/:id/user/:idUser', (req, res) => {
      try {
        if (!req.params.id || !req.params.idUser) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.PhotoListModel.findById(req.params.id, function (err, doc) {
          if (doc.user === req.params.idUser) {
            res.status(200).json(doc || {})
          } else {
            res.status(403).json({
              status: 403,
              message: 'You are not in event.'
            })
          }
        })
      } catch (err) {
        console.error(`[ERROR] show:photo-list/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  update() {
    this.app.put('/photo-list/update/:id/user/:idUser', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.PhotoListModel.findById(req.params.id, function (err, doc) {
          if (doc.user === req.params.idUser) {
            doc.updateOne(
              req.body,
              // options
            ).then((photoListUpdated) => {
              res.status(200).json(photoListUpdated || {})
            }).catch((err) => {
              res.status(400).json({
                status: 400,
                message: err
              })
            })
          } else {
            res.status(403).json({
              status: 403,
              message: 'You are not the owner.'
            })
          }
        })
      } catch (err) {
        console.error(`[ERROR] update:photo-list/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  run() {
    this.create();
    this.delete();
    this.show();
    this.update();
  }
}
