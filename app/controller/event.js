const EventModel = require('../model/event.js')
const GroupModel = require('../model/group.js')

module.exports = class Event {
  constructor(app, connect) {
    this.app = app;
    this.GroupModel = connect.model('Group', GroupModel)
    this.EventModel = connect.model('Event', EventModel)

    this.run();
  }

  findGroup(idGroup, res) {
    return this.GroupModel.findById(idGroup).catch((err) => {
      res.status(404).json({
        status: 404,
        message: 'Group not found'
      })

      return null;
    });
  }

  create() {
    this.app.post('/event/group/:id/user/:idUser', async (req, res) => {
      try {
        const eventModel = new this.EventModel(req.body);
        let groupData = '';

        groupData = await this.findGroup(req.params.id, res);

        let obj = req.body;

        if (groupData !== null) {
          obj = Object.assign(req.body, { adminList : [ req.params.idUser ] });

          let flag = false;

          if (groupData.adminList.includes(req.params.idUser)) {
            flag = true;
            groupData.adminList = groupData.adminList.filter(item => item !== req.params.idUser);
          }

          if (groupData.memberList.includes(req.params.idUser)) {
            flag = true;

            if (groupData.memberCanEvent) {
              groupData.memberList = groupData.memberList.filter(item => item !== req.params.idUser);
            } else {
              res.status(403).json({
                code: 403,
                message: 'Member cannot create event in this group.'
              })

              return null;
            }
          }

          if (false === flag) {
            res.status(404).json({
              code: 404,
              message: 'Not in group'
            })

            return null;
          }

          let memberList = groupData.adminList.concat(groupData.memberList);

          obj = Object.assign(req.body, { memberList : memberList });

          const eventModel = new this.EventModel(obj);

          eventModel.save().then((event) => {
            res.status(200).json(event || {})
          }).catch(() => {
            res.status(200).json({})
          })
        }
      } catch (err) {
        console.log(`[ERROR] event/create SearchGroup -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  delete() {
    this.app.delete('/delete/event/:id/user/:idUser', async (req, res) => {
      if (!req.params.id || !req.params.idUser) {
        res.status(400).json({
          status: 400,
          message: 'Bad Request : Please use a id in the query string parameter'
        })

        return;
      }

      try {
        this.EventModel.findById(req.params.id, function (err, doc) {
          if (doc.adminList.includes(req.params.idUser)) {
            doc.deleteOne(
              req.body,
              // options
            ).then((eventUpdated) => {
              res.status(200).json(eventUpdated || {})
            }).catch((err) => {
              res.status(400).json({
                status: 400,
                message: err
              })
            })

          } else {
            res.status(403).json({
              status: 403,
              message: 'Not an administrator of this event'
            })
          }
        })
      } catch (err) {
        console.log(`[ERROR] event/delete -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  show() {
    this.app.get('/event/show/:id/user/:idUser', (req, res) => {
      try {
        if (!req.params.id || !req.params.idUser) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.EventModel.findById(req.params.id, function (err, doc) {
          if (!doc.private) {
            res.status(200).json(doc || {})
          } else {
            if (doc.adminList.includes(req.params.idUser) || doc.memberList.includes(req.params.idUser)) {
              res.status(200).json(doc || {})
            } else {
              res.status(403).json({
                status: 403,
                message: 'The event is private and you are not in.'
              })
            }
          }
        })
      } catch (err) {
        console.error(`[ERROR] show:event/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  update() {
    this.app.put('/event/update/:id/user/:idUser', (req, res) => {
      try {
        if (!req.params.id || !req.params.idUser) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.EventModel.findById(req.params.id, function(err, doc) {
          if (doc.adminList.includes(req.params.idUser)) {
            doc.updateOne(
              req.body,
              // options
            ).then((eventUpdated) => {
              res.status(200).json(eventUpdated || {})
            }).catch((err) => {
              res.status(400).json({
                status: 400,
                message: err
              })
            })

          } else {
            res.status(403).json({
              status: 403,
              message: 'Not an administrator'
            })
          }
        })
      } catch (err) {
        console.error(`[ERROR] update:event/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  async run() {
    this.create();
    this.delete();
    this.show();
    this.update();
  }
}
