const UserModel = require('../model/user.js')

module.exports = class User {
  constructor(app, connect) {
    this.app = app;
    this.UserModel = connect.model('User', UserModel)

    this.run();
  }

  update() {
    this.app.put('/user/update/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.UserModel.findByIdAndUpdate(
          req.params.id,
          req.body,
          // options
        ).then((userUpdated) => {
          res.status(200).json(userUpdated || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] update:users/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  create() {
    this.app.post('/user/', (req, res) => {
      try {
        const userModel = new this.UserModel(req.body);

        userModel.save().then((user) => {
          res.status(200).json(user || {})
        }).catch(() => {
          res.status(200).json({})
        })
      } catch (err) {
        console.log(`[ERROR] user/create -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  delete() {
    this.app.delete('/delete/user/:id', async (req, res) => {
      if (!req.params.id) {
        res.status(400).json({
          status: 400,
          message: 'Bad Request : Please use a id in the query string parameter'
        })

        return;
      }

      try {
        const userModel = new this.UserModel(req.body);

        await this.UserModel.findByIdAndDelete(req.params.id);

        res.status(200);

        res.send();
      } catch (err) {
        console.log(`[ERROR] user/delete -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  show() {
    this.app.get('/user/show/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.UserModel.findById(req.params.id).then((user) => {
          res.status(200).json(user || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] show:users/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  run() {
    this.update();
    this.create();
    this.delete();
    this.show();
  }
}
