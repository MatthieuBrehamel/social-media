const TicketUserModel = require('../model/ticketUser.js')

module.exports = class TicketUser {
  constructor(app, connect) {
    this.app = app;
    this.TicketUserModel = connect.model('TicketUser', TicketUserModel)

    this.run();
  }

  create() {
    this.app.post('/ticket-achete/', (req, res) => {
      try {
        const ticketUserModel = new this.TicketUserModel(req.body);

        ticketUserModel.save().then((ticketUser) => {
          res.status(200).json(ticketUser || {})
        }).catch(() => {
          res.status(200).json({})
        })
      } catch (err) {
        console.log(`[ERROR] ticket-achete/create -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  delete() {
    this.app.delete('/delete/ticket-achete/:id', async (req, res) => {
      if (!req.params.id) {
        res.status(400).json({
          status: 400,
          message: 'Bad Request : Please use a id in the query string parameter'
        })

        return;
      }

      try {
        const ticketUserModel = new this.TicketUserModel(req.body);

        await this.TicketUserModel.findByIdAndDelete(req.params.id);

        res.status(200);

        res.send();
      } catch (err) {
        console.log(`[ERROR] ticket-achete/delete -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  show() {
    this.app.get('/ticket-achete/show/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.TicketUserModel.findById(req.params.id).then((ticketUser) => {
          res.status(200).json(ticketUser || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] show:ticket-achete/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  update() {
    this.app.put('/ticket-achete/update/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.TicketUserModel.findByIdAndUpdate(
          req.params.id,
          req.body,
          // options
        ).then((ticketUserUpdated) => {
          res.status(200).json(ticketUserUpdated || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] update:ticket-achete/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  run() {
    this.create();
    this.delete();
    this.show();
    this.update();
  }
}
