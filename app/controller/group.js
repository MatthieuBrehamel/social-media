const GroupModel = require('../model/group.js')

module.exports = class Group {
  constructor(app, connect) {
    this.app = app;
    this.GroupModel = connect.model('Group', GroupModel)

    this.run();
  }

  create() {
    this.app.post('/group/', (req, res) => {
      try {
        const groupModel = new this.GroupModel(req.body);

        groupModel.save().then((group) => {
          res.status(200).json(group || {})
        }).catch(() => {
          res.status(200).json({})
        })
      } catch (err) {
        console.log(`[ERROR] group/create -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  delete() {
    this.app.delete('/group/delete/:id/user/:idUser', async (req, res) => {
      if (!req.params.id || !req.params.idUser) {
        res.status(400).json({
          status: 400,
          message: 'Bad Request : Please use a id in the query string parameter'
        })

        return;
      }

      try {
        this.GroupModel.findById(req.params.id, function (err, doc) {
          if (doc.adminList.includes(req.params.idUser)) {
            doc.deleteOne(
              req.body,
              // options
            ).then((groupUpdated) => {
              res.status(200).json(groupUpdated || {})
            }).catch((err) => {
              res.status(400).json({
                status: 400,
                message: err
              })
            })

          } else {
            res.status(403).json({
              status: 403,
              message: 'Not an administrator'
            })
          }
        })
      } catch (err) {
        console.log(`[ERROR] group/delete -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  show() {
    this.app.get('/group/show/:id/user/:idUser', (req, res) => {
      try {
        if (!req.params.id || !req.params.idUser) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.GroupModel.findById(req.params.id, function (err, doc) {
          if (doc.type === 'public') {
              res.status(200).json(doc || {})
          } else if (doc.type === 'private') {
            if (doc.adminList.includes(req.params.idUser) || doc.memberList.includes(req.params.idUser)) {
              res.status(200).json(doc || {})
            } else {
              res.status(403).json({
                status: 403,
                message: 'The group is private and you are not in.'
              })
            }
          } else if (doc.type === 'secret') {
            if (doc.adminList.includes(req.params.idUser) || doc.memberList.includes(req.params.idUser)) {
              res.status(200).json(doc || {})
            } else {
              res.status(403).json({
                status: 403,
                message: 'The group is secret and you are not in.'
              })
            }
          }
        })
      } catch (err) {
        console.log(`[ERROR] group/show -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  update() {
    this.app.put('/group/update/:id/user/:idUser', (req, res) => {
      try {
        if (!req.params.id || !req.params.idUser) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.GroupModel.findById(req.params.id, function(err, doc) {
          if (doc.adminList.includes(req.params.idUser)) {
            doc.updateOne(
              req.body,
              // options
            ).then((groupUpdated) => {
              res.status(200).json(groupUpdated || {})
            }).catch((err) => {
              res.status(400).json({
                status: 400,
                message: err
              })
            })

          } else {
            res.status(403).json({
              status: 403,
              message: 'Not an administrator'
            })
          }
        })
      } catch (err) {
        console.error(`[ERROR] update:group/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  run() {
    this.create();
    this.delete();
    this.show();
    this.update();
  }
}
