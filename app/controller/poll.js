const PollModel = require('../model/poll.js')

module.exports = class Poll {
  constructor(app, connect) {
    this.app = app;
    this.PollModel = connect.model('Poll', PollModel)

    this.run();
  }

  create() {
    this.app.post('/sondage/', (req, res) => {
      try {
        const pollModel = new this.PollModel(req.body);

        pollModel.save().then((poll) => {
          res.status(200).json(poll || {})
        }).catch(() => {
          res.status(200).json({})
        })
      } catch (err) {
        console.log(`[ERROR] sondage/create -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  delete() {
    this.app.delete('/delete/sondage/:id', async (req, res) => {
      if (!req.params.id) {
        res.status(400).json({
          status: 400,
          message: 'Bad Request : Please use a id in the query string parameter'
        })

        return;
      }

      try {
        const pollModel = new this.PollModel(req.body);

        await this.PollModel.findByIdAndDelete(req.params.id);

        res.status(200);

        res.send();
      } catch (err) {
        console.log(`[ERROR] sondage/delete -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  show() {
    this.app.get('/sondage/show/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.PollModel.findById(req.params.id).then((poll) => {
          res.status(200).json(poll || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] show:sondage/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  update() {
    this.app.put('/sondage/update/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.PollModel.findByIdAndUpdate(
          req.params.id,
          req.body,
          // options
        ).then((pollUpdated) => {
          res.status(200).json(pollUpdated || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] update:sondage/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  run() {
    this.create();
    this.delete();
    this.show();
    this.update();
  }
}
