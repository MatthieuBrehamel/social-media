const TicketingModel = require('../model/ticketing.js')

module.exports = class Ticketing {
  constructor(app, connect) {
    this.app = app;
    this.TicketingModel = connect.model('Ticketing', TicketingModel)

    this.run();
  }

  create() {
    this.app.post('/billeterie/', (req, res) => {
      try {
        const ticketingModel = new this.TicketingModel(req.body);

        ticketingModel.save().then((ticketing) => {
          res.status(200).json(ticketing || {})
        }).catch(() => {
          res.status(200).json({})
        })
      } catch (err) {
        console.log(`[ERROR] billeterie/create -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  delete() {
    this.app.delete('/delete/billeterie/:id', async (req, res) => {
      if (!req.params.id) {
        res.status(400).json({
          status: 400,
          message: 'Bad Request : Please use a id in the query string parameter'
        })

        return;
      }

      try {
        const ticketingModel = new this.TicketingModel(req.body);

        await this.TicketingModel.findByIdAndDelete(req.params.id);

        res.status(200);

        res.send();
      } catch (err) {
        console.log(`[ERROR] billeterie/delete -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  show() {
    this.app.get('/billeterie/show/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.TicketingModel.findById(req.params.id).then((ticketing) => {
          res.status(200).json(ticketing || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] show:billeterie/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  update() {
    this.app.put('/billeterie/update/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.TicketingModel.findByIdAndUpdate(
          req.params.id,
          req.body,
          // options
        ).then((ticketingUpdated) => {
          res.status(200).json(ticketingUpdated || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] update:billeterie/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  run() {
    this.create();
    this.delete();
    this.show();
    this.update();
  }
}
