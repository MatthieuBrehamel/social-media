const TicketModel = require('../model/ticket.js')

module.exports = class Ticket {
  constructor(app, connect) {
    this.app = app;
    this.TicketModel = connect.model('Ticket', TicketModel)

    this.run();
  }

  create() {
    this.app.post('/ticket/', (req, res) => {
      try {
        const ticketModel = new this.TicketModel(req.body);

        ticketModel.save().then((ticket) => {
          res.status(200).json(ticket || {})
        }).catch(() => {
          res.status(200).json({})
        })
      } catch (err) {
        console.log(`[ERROR] ticket/create -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  delete() {
    this.app.delete('/delete/ticket/:id', async (req, res) => {
      if (!req.params.id) {
        res.status(400).json({
          status: 400,
          message: 'Bad Request : Please use a id in the query string parameter'
        })

        return;
      }

      try {
        const ticketModel = new this.TicketModel(req.body);

        await this.TicketModel.findByIdAndDelete(req.params.id);

        res.status(200);

        res.send();
      } catch (err) {
        console.log(`[ERROR] ticket/delete -> ${err}`)

        res.status(400).json({
          code: 400,
          message: 'Bad Request'
        })
      }
    })
  }

  show() {
    this.app.get('/ticket/show/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.TickeModel.findById(req.params.id).then((ticket) => {
          res.status(200).json(ticket || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] show:ticket/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  update() {
    this.app.put('/ticket/update/:id', (req, res) => {
      try {
        if (!req.params.id) {
          res.status(400).json({
            status: 400,
            message: 'Bad Request : Please use a id in the query string parameter'
          })

          return;
        }

        this.TicketModel.findByIdAndUpdate(
          req.params.id,
          req.body,
          // options
        ).then((ticketUpdated) => {
          res.status(200).json(ticketUpdated || {})
        }).catch((err) => {
          res.status(400).json({
            status: 400,
            message: err
          })
        })
      } catch (err) {
        console.error(`[ERROR] update:ticket/:id -> ${err}`)

        res.status(500).json({
          status: 500,
          message: 'Internal Server Error'
        })
      }
    })
  }

  run() {
    this.create();
    this.delete();
    this.show();
    this.update();
  }
}
