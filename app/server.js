// Dependencies
const express = require('express');
const mongoose = require('mongoose');

// Dependencies middleware express
const bodyParser = require('body-parser');

// Core
const config = require('./config.js');
const route = require('./controller/route.js');

module.exports = class Server {
  constructor() {
    this.app = express();
    this.config = config[process.argv[2]] || config.development;
  }

  dbConnect() {
    const host = this.config.mongodb.host;
    const connect = mongoose.createConnection(host);

    connect.on('error', (err) => {
      setTimeout(() => {
        console.log(`[ERROR] user api dbConnect -> ${err}`)
        this.connect = this.dbConnect(host);
      }, 5000);
    })

    connect.on('disconnected', (err) => {
      setTimeout(() => {
        console.log(`[DISCONNECTED] user api dbConnect -> mongo disconnected`)
        this.connect = this.dbConnect(host)
      }, 5000);
    })

    connect.on('SIGNINT', (err) => {
      setTimeout(() => {
        console.log(`[API END PROCESS] user api dbConnect -> close mongo connection`)
        process.exit(0)
      }, 5000);
    })

    return connect;
  }

  /**
   * Middleware
   */
  middleware() {
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
  }

  route() {
    new route.User(this.app, this.connect);
    new route.Event(this.app, this.connect);
    new route.Group(this.app, this.connect);
    new route.Chat(this.app, this.connect);
    new route.PhotoList(this.app, this.connect);
    new route.Ticketing(this.app, this.connect);
    new route.Ticket(this.app, this.connect);
    new route.Poll(this.app, this.connect);
    this.app.use((req, res) => {
      res.status(404).json({
        code: 404,
        message: 'Not Found'
      })
    })
  }

  /**
   * Run
   */
  run() {
    try {
      this.connect = this.dbConnect();
      this.middleware();
      this.route();
      this.app.listen(this.config.express.port)
    } catch (err) {
      console.error(`[ERROR Server -> ${err}`);
    }
  }
}
