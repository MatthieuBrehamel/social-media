My Social Network permet de 

Créer/modifier/supprimer/visualiser un groupe

    name: String,
    description: String,
    icon: String,
    picture: String,
    type: String,
    memberCanPost: Boolean,
    memberCanEvent: Boolean,
    memberList: Array,
    adminList: Array

Créer/modifier/supprimer/visualiser un evennement

    name: String,
    description: String,
    startDate: Date,
    endDate: Date,
    city: String,
    picture: String,
    private: Boolean,
    memberList: Array,
    adminList: Array

Créer/modifier/supprimer/visualiser un utilisateur

    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    index: true,
    required: 'Email address is required',
    validate: [validateEmail, 'Please fill a valid email address'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
    }

Créer/modifier/supprimer/visualiser un sondage

    event: String,
    question: Array,
    answer: Array,

Créer/modifier/supprimer/visualiser des commentaires

    parentMessage: String,
    date: Date,
    group: String,
    event: String,
    Content: String,
    user: String

Créer/modifier/supprimer/visualiser des albums photos

    event: String,
    user: String,
    photoList: Array,

Créer/modifier/supprimer/visualiser des billeteries

    event: String,
    ticketType: Array,

